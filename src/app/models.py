from app.internal.models.admin_user import AdminUser
from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=255)
    iso_code = models.CharField(max_length=10)


class City(models.Model):
    name = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()
    country = models.ForeignKey('app.Country', on_delete=models.RESTRICT, related_name='cities')


class Place(models.Model):
    name = models.CharField(max_length=255)
    info = models.TextField(blank=True)
    city = models.ForeignKey('app.City', on_delete=models.RESTRICT, related_name='places')


class Person(models.Model):
    uid = models.IntegerField(unique=True)
    name = models.CharField(max_length=255)
    bio = models.TextField(null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True)
    email = models.EmailField(null=True)
    city = models.ForeignKey('app.City', on_delete=models.RESTRICT, related_name='people')
    favorite_places = models.ManyToManyField('app.Place')


def __str__(self):
    return f'{self.name}'


class Meta:
    verbose_name = 'Person'
    verbose_name_plural = 'People'
