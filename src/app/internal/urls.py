from django.urls import path
from .views import me_view

urlpatterns = [
    path('me/<int:user_id>/', me_view, name='me'),
]
