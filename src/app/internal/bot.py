from telegram.ext import ApplicationBuilder
from config.settings import API_TOKEN, logger
from .transport.bot.handlers import start_handler, set_phone_handler, me_handler


def main():
    application = ApplicationBuilder().token(API_TOKEN).build()

    application.add_handler(start_handler)
    application.add_handler(set_phone_handler)
    application.add_handler(me_handler)

    # Start the Bot
    application.run_polling()
    logger.info('bot started')


if __name__ == '__main__':
    main()
