from django.http import HttpResponse
from ..models import Person  # Не знаю как, но я упустил это раньше, из-за этого в обработках команд у меня мягко говоря плохой код


def me_view(request, user_id):
    user_info = Person.objects.get(uid=user_id)
    country = user_info.city.country.namentry
    response = f'Hi, here\'s all I know about you:' \
               f'\nYour name - {user_info.name}' \
               f'\nYour phone - {user_info.phone_number}' \
               f'\nYour from - {country}' \
               f'\nAnd you haven\'t filled in the information about yourself (But don\'t rush to do it, it hasn\'t been implemented yet)'
    return HttpResponse(response)
