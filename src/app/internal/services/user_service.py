import sqlite3
import re
from config.settings import BASE_DIR


def create_connection():
    conn = sqlite3.connect(f'{BASE_DIR}/db.sqlite3')
    return conn


def add_user(uid, name, language_code):
    conn = create_connection()
    c = conn.cursor()
    c.execute('''SELECT * from app_person WHERE uid=?''', (uid, ))
    persons = c.fetchall()
    if persons:
        return -1
    c.execute('''SELECT id from app_country WHERE iso_code=?''', (language_code, ))
    country = c.fetchone()
    c.execute('''INSERT INTO app_person(uid, name, city_id) VALUES(?,?,?)''', (uid, name, country[0]))
    conn.commit()
    conn.close()


def validity_number(phone):
    if re.match(r"^(?:\+?44)?[+078]\d{9,13}$", phone):
        return phone
    return -1


def add_phone(uid, phone):
    conn = create_connection()
    c = conn.cursor()
    sql_update_query = """UPDATE app_person SET phone_number = ? where uid = ?"""
    data = (phone, uid)
    c.execute(sql_update_query, data)
    conn.commit()
    conn.close()


def check_number(uid):
    conn = create_connection()
    c = conn.cursor()
    c.execute('''SELECT phone_number from app_person WHERE uid=?''', (uid,))
    phone = c.fetchone()
    return phone == (None,) or phone is None


def get_info(uid):
    conn = create_connection()
    c = conn.cursor()
    c.execute('''SELECT * from app_person WHERE uid=?''', (uid,))
    persons = c.fetchone()
    return persons


def get_country(country_id):
    conn = create_connection()
    c = conn.cursor()
    c.execute('''SELECT * from app_country WHERE id=?''', (country_id,))
    country = c.fetchone()
    return country