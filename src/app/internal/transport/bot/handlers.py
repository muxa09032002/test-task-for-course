from telegram.ext import CommandHandler

from commands import start, set_phone, me

start_handler = CommandHandler("start", start)
set_phone_handler = CommandHandler("set_phone", set_phone)
me_handler = CommandHandler("me", me)
