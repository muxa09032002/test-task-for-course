from loguru import logger
from telegram import Update
from telegram.ext import ContextTypes
from app.internal.services.user_service import add_user, add_phone, validity_number, check_number, get_info, get_country


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = update.message.from_user
    logger.info('get start message')
    if update.message:
        res = add_user(user.id, user.first_name, user.language_code)
        await update.message.reply_text(f"Hi {user.username}")
        if res == -1:
            await update.message.reply_text('You are already registered!')


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message_args = context.args
    phone = validity_number(message_args[0]) if len(message_args) > 0 else -1
    user = update.message.from_user
    phone = ''.join([d for d in phone if d.isdigit()]) if phone != -1 else -1
    if phone != -1:
        add_phone(user.id, phone)
        await update.message.reply_text('Your phone is saved!')
    else:
        await update.message.reply_text('You have entered an invalid phone number, please write: /set_phone <phone_number>')


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = update.message.from_user
    number_entered = check_number(user.id)
    if number_entered:
        if update.message:
            await update.message.reply_text('You have not filled in your phone number, access to the team is denied')
        return
    res = get_info(user.id)
    country = get_country(res[5])[1]
    if update.message:
        await update.message.reply_text(f'Hi, here\'s all I know about you:'
                                        f'\nYour name - {res[1]}'
                                        f'\nYour phone - {res[3]}'
                                        f'\nYour from - {country}'
                                        f'\nAnd you haven\'t filled in the information about yourself (But don\'t rush to do it, it hasn\'t been implemented yet)')


# TODO: Add the endpoint me function and refactoring
